import pytest

import bcrypt_example

@pytest.fixture
def client():

    with bcrypt_example.app.test_client() as client:
        yield client

def test_static_html(client):
    """Serves the static html page and the root path"""

    rv = client.get('/')
    assert b'<title>Single-Page Login and Post</title>' in rv.data
